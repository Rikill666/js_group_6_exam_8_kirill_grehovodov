import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";
import axiosQuote from "../../axios-api";
import {NavLink} from "react-router-dom";
import Quote from "./Quote/Quote";
import {CATEGORIES} from "../../constants";

class Quotes extends Component {
    state = {
        quotes: {},
    };
    requestData = async() => {
        let url = '/quotes.json';
        if(this.props.match.params.name){
            url += `?orderBy="category"&equalTo="${this.props.match.params.name}"`;
        }
        const response = await axiosQuote.get(url);
        this.setState({quotes:response.data});

    };
    async componentDidMount() {
        this.requestData();
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.match.params.name !== this.props.match.params.name){
            return this.requestData();
        }
    }

    async deleteQuote(id) {
        await axiosQuote.delete('/quotes/' + id + ".json");
        this.requestData();
    }

    render() {
        return (
            <Container className="mt-3">
                <Row>
                    <Col lg="3" md="4" xs="6">
                        <ul>
                            {CATEGORIES.map(c => (
                                <li key={c.id}>
                                    <NavLink to={'/quotes/' + c.id}>{c.title}</NavLink>
                                </li>
                            ))}
                        </ul>
                    </Col>
                    <Col lg="9" md="8" xs="6">
                        <div>
                            {this.state.quotes? Object.keys(this.state.quotes).map(id => (
                                <Quote
                                    key={id}
                                    text={this.state.quotes[id].text}
                                    author={this.state.quotes[id].author}
                                    id={id}
                                    deleteQuote={() => this.deleteQuote(id)}
                                />
                            )): <h1>No Quotes</h1>}
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Quotes;