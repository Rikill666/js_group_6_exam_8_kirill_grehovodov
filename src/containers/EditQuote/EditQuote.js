import React, {Component} from 'react';
import axiosQuote from "../../axios-api";
import FormsForQuote from "../../components/FormsForQuote/FormsForQuote";

class EditQuote extends Component {
    state={
        author:"",
        text:"",
        category:"",
    };
    valueChanged = event => this.setState({[event.target.name]: event.target.value});
    quoteHandler = async event => {
        event.preventDefault();
        const quote = {
            author: this.state.author,
            text: this.state.text,
            category: this.state.category
        };
            await axiosQuote.put('/quotes/'+ this.props.match.params.id +'.json', quote);
            this.props.history.goBack();
    };

    async componentDidMount() {
        if(this.props.match.params.id){
            const response = await axiosQuote.get('/quotes/' + this.props.match.params.id+ '.json');
            const quote = response.data;
            this.setState({author:quote.author, text:quote.text, category:quote.category});
        }
    }
    render() {
        return (
            <div>
                <FormsForQuote
                    author={this.state.author}
                    text={this.state.text}
                    category={this.state.category}
                    valueChanged={(e) => this.valueChanged(e)}
                    quoteHandler={(e) => this.quoteHandler(e)}
                />
            </div>
        );
    }
}

export default EditQuote;