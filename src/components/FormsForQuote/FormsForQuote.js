import React from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {CATEGORIES} from "../../constants";

const FormsForQuote = (props) => {
    return (
        <Form onSubmit={props.quoteHandler}>
            <FormGroup>
                <Label for="exampleSelect">Category</Label>
                <Input
                    type="select"
                    name="category"
                    id="exampleSelect"
                    onChange={props.valueChanged}
                    value={props.category}
                >
                    {CATEGORIES.map((c) => (
                        <option key={c.id} value={c.id}>{c.title} </option>
                    ))}
                </Input>
            </FormGroup>
            <FormGroup>
                <Label for="exampleAuthor">Author</Label>
                <Input type="text"
                       name="author"
                       id="exampleAuthor"
                       onChange={props.valueChanged}
                       value={props.author}
                />
            </FormGroup>
            <FormGroup>
                <Label for="exampleText">Quote text</Label>
                <Input type="textarea"
                       name="text"
                       id="exampleText"
                       onChange={props.valueChanged}
                       value={props.text}
                />
            </FormGroup>
            <Button>Save</Button>
        </Form>
    );
};

export default FormsForQuote;