import React from 'react';
import {Card, CardText, CardTitle, Col, NavLink, Row} from "reactstrap";
import {NavLink as NavRoute} from "react-router-dom";
import "./Quote.css";

const Quote = (props) => {
    return (
        <div>
            <Card body>
                <Row>
                    <Col lg="9" md="9" xs="8">
                        <CardText>"{props.text}"</CardText>
                        <CardTitle>---{props.author}</CardTitle>
                    </Col>
                    <Col lg="3" md="3" xs="4" >
                        <div className="delEdit">
                            <NavLink className="edit" tag={NavRoute} to={'/quotes/' + props.id + '/edit'} exact> <i className="fas fa-edit"/></NavLink>
                            <i onClick={props.deleteQuote} className="fas fa-times"/>
                        </div>
                    </Col>
                </Row>
            </Card>
        </div>
    );
};

export default Quote;