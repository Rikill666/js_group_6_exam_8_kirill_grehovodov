import React, {Component} from 'react';
import axiosQuote from "../../axios-api";
import FormsForQuote from "../../components/FormsForQuote/FormsForQuote";
import {CATEGORIES} from "../../constants";

class AddQuote extends Component {
    state={
        author:"",
        text:"",
        category:CATEGORIES[0].id,
    };

    valueChanged = event => this.setState({[event.target.name]: event.target.value});

    quoteHandler = async event => {
        event.preventDefault();
        const quote = {
            author: this.state.author,
            text: this.state.text,
            category: this.state.category
        };
            await axiosQuote.post('/quotes.json', quote);
            this.props.history.replace('/');
    };
    render() {
        return (
            <div>
                <FormsForQuote
                    author={this.state.author}
                    text={this.state.text}
                    category={this.state.category}
                    valueChanged={(e) => this.valueChanged(e)}
                    quoteHandler={(e) => this.quoteHandler(e)}
                />
            </div>
        );
    }
}

export default AddQuote;