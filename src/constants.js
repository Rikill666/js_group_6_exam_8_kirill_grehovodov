export const CATEGORIES = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Motivational', id: 'motivational'},
    {title: 'Humour', id: 'humour'},
    {title: 'Famous people', id: 'famous-people'},
    {title: 'Saying', id: 'saying'},
];