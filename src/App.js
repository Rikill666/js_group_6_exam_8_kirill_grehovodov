import React from 'react';
import {Container} from "reactstrap";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Quotes from "./containers/Quotes/Quotes";
import AddQuote from "./containers/AddQuote/AddQuote";
import EditQuote from "./containers/EditQuote/EditQuote";
import NavMenu from "./components/NavMenu/NavMenu";

function App() {
    return (
        <BrowserRouter>
            <NavMenu/>
            <Container>
                <Switch>
                    <Route path="/" exact component={Quotes}/>
                    <Route path="/add-quote" component={AddQuote}/>
                    <Route path="/quotes/:id/edit" component={EditQuote}/>
                    <Route path="/quotes/:name" component={Quotes}/>
                    <Route render={() => <h1>Not Found</h1>}/>
                </Switch>
            </Container>
        </BrowserRouter>
    );
}

export default App;
