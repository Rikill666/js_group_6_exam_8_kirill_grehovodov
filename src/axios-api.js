import axios from "axios";

const axiosQuote = axios.create({
    baseURL: 'https://js6homework63.firebaseio.com/'
});

export default axiosQuote;