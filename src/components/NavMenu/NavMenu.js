import React from 'react';
import {Container, Nav, Navbar, NavbarBrand, NavItem, NavLink} from "reactstrap";
import {NavLink as NavRoute} from "react-router-dom";

const NavMenu = () => {
    return (
        <div>
            <Navbar color="light" light expand="md">
                <Container>
                    <NavbarBrand tag={NavRoute} to="/">Quotes Central</NavbarBrand>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={NavRoute} to="/" exact>Quotes</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={NavRoute} to="/add-quote" exact>Submit new quote</NavLink>
                        </NavItem>
                    </Nav>
                </Container>
            </Navbar>
        </div>
    );
};

export default NavMenu;